# pixi scripts for [sunvox](https://warmplace.ru/soft/sunvox/)

## install

* get the [pixilang app](https://warmplace.ru/soft/pixilang/)
* [download](https://gitlab.com/unlessgames/unless_sunvox/-/archive/main/unless_sunvox-main.zip) this repo
* unzip into the installation directory of pixilang
* run scripts with pixilang

# sampler_banker

generates a sunvox project file with a bank of Samplers and MultiSynths corresponding to each sample inside a chosen directory. I made it to speed up the process of transfering exported samples from the [Koala Sampler](https://www.koalasampler.com/) but could work with any folder that has **wav** files named with numbers like `0.wav, 1.wav`. 

samples will be mapped to keys by their names, 0 becomes C4, 1 becomes C#4 etc...

the output project will be named the same as the input folder (with .sunvox appended)

at the start you have to select any sample inside the target folder (the built-in file picker cannot pick folders only files so this is how it works now)

once that is done it will generate the sunvox file instantly, you can exit with the back button or test the pads inside pixi to make sure you have picked the right samples.

I might add "in-app customizations" later on but for now you can simply edit the script to change stuff (the starting note is at the top of the file).
