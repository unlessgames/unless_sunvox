PRECISION( HIGHP, float )
// uniform sampler2D g_texture;
// uniform vec4 g_color;
uniform int _scope[ 512 ];
uniform int _length;
IN vec2 tex_coord_var;
void main()
{
    vec2 p = tex_coord_var;
    float y = _scope[floor(p.x * float(_length))] / float(0x8000);
    // tc1.y += g_buf[ int( tc1.x * 512.0 ) ].y;
    // tc2.y += g_buf[ int( tc1.x * 512.0 ) ].x;
    // gl_FragColor = texture2D( g_texture, tc1 ) * vec4( 1, 0, 0, 1 ) + texture2D( g_texture, tc2 ) * vec4( 0, 1, 0, 1 );
    float d = abs(y - p.y + 0.5);
    // d = abs(y) < 0.02 ? 0.0 : d;
    d = d < 0.02 ? 1.0 : 0.0;
    // float d = abs(y - p.y + 0.5) < 0.01 ? 1.0 : 0.0;
    gl_FragColor = vec4(vec3(d * 0.5),1.0);
    // gl_FragColor = vec4(vec3(y * 0.5 + 0.5),1.0);
}