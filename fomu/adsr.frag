PRECISION( HIGHP, float )
uniform sampler2D g_texture;
uniform float _a;
uniform float _d;
uniform float _s;
uniform float _r;
uniform float _v;
uniform float _l;
uniform vec4 g_color;
IN vec2 tex_coord_var;
void main()
{
	float sl = 1.0 / _l;
	float a = _a * sl;
	float d = _d * sl;
	float r = _r * sl;

    vec2 tc = tex_coord_var;
    // int i = floor(tc.x * 0.25);

    float y = 1.0 - tc.y;

    float x = 0.0;
    float p = 0;
    if(tc.x < a){
		y = y * (1.0 / (_v));
    	x = tc.x / a;
    	p = (y) - (x);
    }else if(tc.x < a + d){
    	x = (tc.x - a) / d;
		y = y * (1.0 / _v) - mix(0.0, _s, x);
		// y = y * (1.0 / mix(_v, _v * _s, x));
    	p = x - (1.0 - y);
    }else if(tc.x < a + d + r){
		y = 1.0 - y * (1.0 / (_v * _s));
		y = pow(abs(y - 1.0), 2.0);
		y = 1.0 - y;
    	x = (((tc.x) - a - d) / r);
    	p = (x) - (y);
    }else{
    	p = 1.0;
    }
    float s = step(abs(p), 0.02);// * (1.0 + tc.y * 2.0));
    gl_FragColor = vec4(s, 0, 0, 1.0 );
    int t = tc.x * 1024;
    uint c = (((13 * t)/50)*(((75 * t)/50)>>5|t>>8))>>(((31*t)/50)>>16);
    gl_FragColor = vec4(vec3(float(c) / 255.0), 1.0);
}